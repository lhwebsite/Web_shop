package cn.item.mapper;

import cn.hmall.pojo.item.Item;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface itemMapper extends BaseMapper<Item> {
}
