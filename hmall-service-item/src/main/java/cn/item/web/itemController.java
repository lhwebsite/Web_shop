package cn.item.web;

import cn.hmall.dto.common.PageDTO;
import cn.hmall.dto.common.ResultDTO;
import cn.hmall.dto.item.SearchItemDTO;
import cn.hmall.pojo.item.Item;
import cn.hmall.pojo.item.ItemDoc;
import cn.item.service.itemService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/item")
public class itemController {

    @Autowired
    private itemService itemservice;

    @Autowired
    private RabbitTemplate rabbitTemplate;
    /**
     * 分页查询
     * @param searchItemDTO
     * @return
     */
    @PostMapping ("/list")
    public PageDTO<Item> selectList(@RequestBody SearchItemDTO searchItemDTO){
        return itemservice.selectList(searchItemDTO);
    }

    /**
     * 根据id查询商品
     * @param id
     * @return
     */
    @GetMapping ("/{id}")
    public Item selectItemById(@PathVariable("id") Long id){
        return itemservice.selectItemById(id);
    }

    @PostMapping
    public ResultDTO addItem(@RequestBody Item item){
        itemservice.addItem(item);
        return ResultDTO.ok();
    }

    @PutMapping("/status/{id}/{status}")
    public ResultDTO updateStatus(@PathVariable("id") Long id,@PathVariable("status") Integer status){
        try {
            itemservice.updateStatus(id, status);
            if(status == 1){
                rabbitTemplate.convertAndSend("shop.topicExchange","up",id);
            }else if(status == 2){
                rabbitTemplate.convertAndSend("shop.topicExchange","down",id);
            }
            return ResultDTO.ok();
        } catch (Exception e) {
            e.printStackTrace();
            return ResultDTO.error("修改上下架状态失败,原因是:" + e.getMessage());
        }
    }

    /**
     * 修改商品信息
     * @param item
     * @return
     */
    @PutMapping
    public ResultDTO updateItem(@RequestBody Item item){
        try {
            itemservice.updateItem(item);
            return ResultDTO.ok();
        } catch (Exception e) {
            e.printStackTrace();
            return ResultDTO.error("修改商品失败,原因是:" + e.getMessage());
        }
    }

    /**
     * 根据id删除商品
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    public ResultDTO deleteItemById(@PathVariable("id") Long id){
        try {
            itemservice.deleteItemById(id);
            return ResultDTO.ok();
        } catch (Exception e) {
            e.printStackTrace();
            return ResultDTO.error("删除商品失败,原因是:" + e.getMessage());
        }
    }

    /**
     * 扣减商品库存
     * @param id
     * @param num
     * @return
     */
    @PutMapping("/stock/{id}/{num}")
    public ResultDTO deleteStock(@PathVariable("id") Long id,@PathVariable("num") Integer num){
        itemservice.deleteStock(id,num);
        return ResultDTO.ok();
    }

    /**
     * 新增商品库存
     * @param id
     * @param num
     * @return
     */
    @RequestMapping("/stock/add/{id}/{num}")
    public ResultDTO addStock(@PathVariable("id") Long id,@PathVariable("num") Integer num){
        itemservice.addStock(id,num);
        return ResultDTO.ok();
    }
}
