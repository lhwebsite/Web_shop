package cn.item.service.impl;

import cn.hmall.dto.common.PageDTO;
import cn.hmall.dto.item.SearchItemDTO;
import cn.hmall.pojo.item.Item;
import cn.hmall.pojo.item.ItemDoc;
import cn.item.mapper.itemMapper;
import cn.item.service.itemService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.mustachejava.util.Wrapper;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
public class itemServiceImpl extends ServiceImpl<itemMapper,Item> implements itemService {

    @Autowired
    private RabbitTemplate rabbitTemplate;
    /**
     * 实现分页查询
     * @param dto
     * @return
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public PageDTO<Item> selectList(SearchItemDTO dto) {
        LambdaQueryWrapper<Item> wrapper = Wrappers.lambdaQuery();
        if(dto.getName() != null){
            wrapper.like(Item::getName,dto.getName());
        }
        if(dto.getBrand() != null){
            wrapper.like(Item::getBrand,dto.getBrand());
        }
        if(dto.getCategory() != null){
            wrapper.like(Item::getCategory,dto.getCategory());
        }
        if(dto.getBeginDate() != null){
            wrapper.ge(Item::getCreateTime,dto.getBeginDate());
        }
        if(dto.getEndDate() != null){
            wrapper.le(Item::getCreateTime,dto.getEndDate());
        }
        Page<Item> pages = new Page<>(dto.getPage(), dto.getSize());
        Page<Item> page = this.page(pages, wrapper);
        return new PageDTO<Item>(page.getTotal(),page.getRecords());
    }

    /**
     * 根据id查询商品
     * @param id
     * @return
     */
    @Override
    public Item selectItemById(Long id) {
        if(id == null){
            throw new RuntimeException("id不能为空");
        }
        return this.getById(id);
    }

    /**
     * 新增商品
     * @param item
     */
    @Override
    public void addItem(Item item) {
        //传入的数据不包含状态，因此要把status设置为1
        item.setStatus(1);
        this.save(item);
    }

    /**
     * 根据id删除商品
     * @param id
     */
    @Override
    public void deleteItemById(Long id) {
        this.removeById(id);
    }

    /**
     * 扣减商品库存
     * @param id
     * @param num
     */
    @Override
    public void deleteStock(Long id, Integer num) {
        if (id != null && num != null) {
            UpdateWrapper<Item> itemUpdateWrapper = new UpdateWrapper<>();
            itemUpdateWrapper.eq("id", id).setSql("stock=stock-" + num);
            this.update(itemUpdateWrapper);
        }
    }

    /**
     * 增加商品库存
     * @param id
     * @param num
     */
    @Override
    public void addStock(Long id, Integer num) {
        if (id != null && num != null) {
            UpdateWrapper<Item> itemUpdateWrapper = new UpdateWrapper<>();
            itemUpdateWrapper.eq("id", id).setSql("stock=stock+" + num);
            this.update(itemUpdateWrapper);
        }
    }

    /**
     * 上下架
     * @param id
     * @param status
     */
    @Override
    public void updateStatus(Long id, Integer status) {
        if(id == null || status == null){
            throw new RuntimeException("id或status不能为空");
        }
        if (status != 2 && status != 1) {
            throw new RuntimeException("状态错误");
        }
        LambdaUpdateWrapper<Item> warpper = Wrappers.lambdaUpdate();
        warpper.eq(Item::getId,id);
        warpper.set(Item::getStatus,status);
        this.update(null,warpper);
    }

    /**
     * 修改商品信息
     * @param item
     */
    @Override
    public void updateItem(Item item) {
        this.updateById(item);
    }


}
