package cn.item.service;

import cn.hmall.dto.common.PageDTO;
import cn.hmall.dto.item.SearchItemDTO;
import cn.hmall.pojo.item.Item;
import cn.hmall.pojo.item.ItemDoc;
import org.springframework.stereotype.Service;


public interface itemService {
    /**
     * 修改商品信息
     * @param item
     */
    void updateItem(Item item);

    /**
     * 上下架
     * @param id
     * @param status
     */
    void updateStatus(Long id, Integer status);

    /**
     * 分页查询
     * @param searchItemDTO
     * @return
     */
    PageDTO<Item> selectList(SearchItemDTO searchItemDTO);

    /**
     * 根据id查询商品
     * @param id
     * @return
     */
    Item selectItemById(Long id);

    /**
     * 新增商品
     * @param item
     */
    void addItem(Item item);

    /**
     * 根据id删除商品
     * @param id
     */
    void deleteItemById(Long id);


    /**
     * 减少商品库存
     * @param id
     * @param num
     */
    void deleteStock(Long id, Integer num);

    /**
     * 增加商品库存
     * @param id
     * @param num
     */
    void addStock(Long id, Integer num);
}
