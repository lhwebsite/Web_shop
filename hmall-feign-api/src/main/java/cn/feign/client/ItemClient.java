package cn.feign.client;
import cn.hmall.dto.common.PageDTO;
import cn.hmall.dto.item.SearchItemDTO;
import cn.hmall.pojo.item.Item;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@FeignClient("itemservice")
public interface ItemClient {
    /**
     * 查询商品列表
     * @param params
     * @return
     */
    @GetMapping("/item/list")
    public PageDTO<Item> selectList(@RequestBody SearchItemDTO params);

    /**
     * 根据id 查询商品信息
     * @param id
     * @return
     */
    @GetMapping("/item/{id}")
    public Item selectItemById(@PathVariable("id")Long id);

    /**
     * 扣减商品库存
     * @param itemId
     * @param num
     */
    @PutMapping("/item/stock/{itemId}/{num}")
    public void deleteStock(@PathVariable("itemId") Long itemId ,@PathVariable("num")Integer num);

    /**
     * 增加商品库存
     * @param id
     * @param num
     */
    @RequestMapping("/item/stock/add/{id}/{num}")
    void addStock(@PathVariable("id") Long id,@PathVariable("num")Integer num);
}
