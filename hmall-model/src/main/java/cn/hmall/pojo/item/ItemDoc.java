package cn.hmall.pojo.item;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
public class ItemDoc {
    private Long id;
    private String name;
    private Long price;
    private String image;
    private String category;
    private String brand;
    private Integer sold;
    private Integer commentCount;
    //是否广告
    private Boolean isAD;
    //自动补全
    private List<String> suggestion = new ArrayList<>(2);

    //自动补全包括 商标 分类
    public ItemDoc(Item item) {
        // 属性拷贝
        BeanUtils.copyProperties(item, this);
        // 补全
        suggestion.add(item.getBrand());
        suggestion.add(item.getCategory());
    }
}